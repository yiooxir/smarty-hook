module.exports = {
    port: 9010,
	projectPath: '/home/sergey/WebstormProjects/work/crm-smarty-v2.x-web',
    entryLink: '/home/sergey/WebstormProjects/gitHuck/123/app',

    // bitbucket ips: '131.103.20.165', '131.103.20.166'
    firewall: ['127.0.0.1', '131.103.20.165', '131.103.20.166'],

	run: ['task_pull','task_makeDeploy', 'task_activateLatest'],
	task_build: {
        args: ''
    },
	task_pull: {
        branch: 'alpha'
    },
    task_makeDeploy: {
        dist: '/home/sergey/WebstormProjects/gitHuck/123',
        dirPrefix: 'v.'
    },
    task_activateLatest: {},
    task_rollback: {}
};
