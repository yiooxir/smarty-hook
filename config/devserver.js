module.exports = {
    port: 9010,
    projectPath: '/var/www/apps/crm-smarty-v2.x-web',
    entryLink: '/var/www/apps/crm-smarty-v2.x-web/applications',

    // bitbucket ips: '131.103.20.165', '131.103.20.166'
    firewall: ['127.0.0.1', '131.103.20.165', '131.103.20.166'],

    run: ['task_pull', 'task_build'],
    task_build: {
        args: '--dev'
    },
    task_pull: {
        branch: 'alpha'
    },
    task_makeDeploy: {
        dist: '/var/www/apps/crm-smarty-v2.x-web',
        dirPrefix: 'v.'
    },
    task_activateLatest: {},
    task_rollback: {}
};
