/**
 * Created by sergey on 31.03.15.
 */

var http = require('http');
var config = require('config');
var async = require('async');
var express = require('express');
var bodyParser = require('body-parser');
var tasks = require('./tasks');
var clc = require('cli-color');

/*
 set up midllware
*/

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


/*
*  tasker. run task in parallel
* */

 function runTasks(callback) {

    async.eachSeries(config.get('run'), function(task, next) {
        // Run task
        console.log(clc.yellow('-------------------------------------'));
        console.log(clc.green('>> start task %s', task));
        tasks[task](next);
    }, callback);
}


/*
    hook listener
*/

app.post('/', function(req, res) {
    if (config.get('firewall').indexOf(req.connection.remoteAddress) == -1) res.end('go fuck');

    console.log(clc.yellow('RECEIVED A NEW HOOK. START TASKS.'));
    runTasks(function(err) {
        if (err) {
            console.error('Fail! %s', err);
            res.end(err.message + err.status + err.stack);
        } else {
            console.log('All tasks are done!');
            res.end('ok');
        }
    });
});



/*
   create server. start server
*/

var port = config.get('port');
http.createServer(app).listen(port, function() {
    console.log('server started on port %s', port);
});
