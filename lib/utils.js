var config = require('config');
var dp = config.get('task_makeDeploy');
var cm = config.get('entryLink');

exports.getVersion = function() {
    return require(config.get('projectPath') + '/' +'package.json').version;
};
exports.getBuildPath = function() {
    return config.get('projectPath') + '/build';
};
exports.getDeployPath = function() {
    return dp.dist + '/' + this.getDeployName();
};
exports.getDeployName = function() {
    return dp.dirPrefix + this.getVersion();
};
exports.getEntryLinkPath = function() {
    return cm.split('/').slice(0,-1).join('/');

};
exports.getEntryLinkName = function() {
    return cm.split('/').pop();
};
