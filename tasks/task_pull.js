var config = require('config');
var exec = require('child_process').exec;
var async = require('async');


module.exports = function(next) {

    var conf = config.get('task_pull');
    var path = config.get('projectPath');

    async.waterfall([
        function(callback) {
            console.log('check current branch ...');
            exec('git rev-parse --abbrev-ref HEAD', {cwd: path}, callback);
        },
        function(stdOut, stdErr, callback) {

            if (stdOut == conf.branch) {
                callback(null, 'ok', null);
            }
            else {
                exec('git checkout ' + conf.branch, {cwd: path}, callback)
            }
        },
        function(stdOut, stdErr, callback) {
            console.log('make git pull from origin: %s ...', conf.branch);
            exec('git pull origin ' + conf.branch, {cwd: path}, callback);
        },
        function(stdOut, stdErr, callback) {
            console.log("pull on '%s' is done", conf.branch);
            callback(null);
        }
    ], function(err) {
        if (err) return next(err);
        next();
    });
};