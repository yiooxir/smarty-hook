var async = require('async');
var fse = require('fs-extra');
var utils = require('../lib/utils');
var sourcePath = utils.getBuildPath();

module.exports = function(next) {

    var version = utils.getVersion();
    var deployPath = utils.getDeployPath();
    var deployName = utils.getDeployName();

    async.waterfall([
        function(callback) {
            if (!version) return callback(new Error('version is required'));

            console.log('create new dir %s ...', deployName);
            fse.mkdirs(deployPath, callback);
        },
        function(path, callback) {
            if (!path) return callback(new Error('error: such directory is already exists'));

            console.log('copy build to dir under version ...');
            fse.copy(sourcePath, deployPath, callback)
        }
    ], function(err) {
        if (err) return next(err);
        next();
    });
};