var exec = require('child_process').exec;
var utils = require('../lib/utils');


module.exports = function(next) {

    exec('ln -s ' + utils.getDeployPath() + ' ' + utils.getEntryLinkName(), {cwd: utils.getEntryLinkPath()}, function(err) {
        if (err) return next(err);
        console.log('new version is activated');
        next();
    })
};

