var config = require('config');
var conf = config.get('task_build');
var exec = require('child_process').exec;

module.exports = function(next) {

    exec('gulp ' + conf.args, {cwd: config.get('projectPath')}, function(err, stdOut) {
        if (err) return next(err);
        console.log(stdOut);
        next();
    });


};